<?php declare(strict_types=1);

// Библиотека
require 'vendor/autoload.php';
use PHPUnit\Framework\TestCase;

require_once 'Complex.php';

class ComplexTest extends TestCase {
    /**
     * @param int|float $realNumber
     * @param int|float $imaginaryNumber
     * @param string $postfix
     *
     * @dataProvider creatingDataProvider
     */
    public function testIsCanBeCreatedAndReturnValidValues(
        int|float $realNumber,
        int|float $imaginaryNumber,
        string    $postfix
    ): void
    {
        $number = new Complex($realNumber, $imaginaryNumber, $postfix);

        self::assertEquals($realNumber, $number->realNumber());
        self::assertEquals($imaginaryNumber, $number->imaginaryNumber());
        self::assertEquals($postfix, $number->postfix());
        self::assertEquals(
            $realNumber . (0 > $imaginaryNumber ? '' : '+') . $imaginaryNumber . $postfix,
            (string)$number
        );
    }

    public function creatingDataProvider(): \Iterator
    {
        yield [0, 0, 'i'];
        yield [1, 0, 'n'];
        yield [0, 1, 'i'];
        yield [0.1, 0.1, 'n'];
        yield [0.1, 0, 'i'];
        yield [0, 0.1, 'n'];
        yield [1.1, 1.1, 'i'];
        yield [1.1, 0, 'n'];
        yield [0, 1.1, 'i'];
        yield [-1, 0, 'n'];
        yield [0, -1, 'i'];
        yield [-0.1, -0.1, 'n'];
        yield [-0.1, 0, 'i'];
        yield [0, -0.1, 'n'];
        yield [-1.1, 1.1, 'i'];
        yield [1.1, -1.1, 'n'];
        yield [-1.1, -1.1, 'i'];
        yield [-1.1, 0, 'n'];
        yield [0, -1.1, 'i'];
    }

    /**
     * @param string $postfix
     * @param string $exceptionClass
     * @param string $exceptionMessage
     * @dataProvider invalidCreatingDataProvider
     */
    public function testIsThrowExceptionOnInvalidPostfix(
        string $postfix,
        string $exceptionClass,
        string $exceptionMessage
    ): void
    {
        $this->expectException($exceptionClass);
        $this->expectExceptionMessage($exceptionMessage);

        new Complex(1, 2, $postfix);

        // заглушка
        self::assertEquals(true, true);
    }

    public function invalidCreatingDataProvider(): \Iterator
    {
        yield [
            '',
            \InvalidArgumentException::class,
            'Необходимо указать постфикс.',
        ];
        yield [
            ' ',
            \InvalidArgumentException::class,
            'Необходимо указать постфикс.',
        ];
    }

    public function testIsImmutable(): void
    {
        $number = new Complex(1, 1);
        $numberClone = clone $number;
        $subNumber = new Complex(1, 0);
        $number->add($subNumber);

        self::assertEquals($numberClone, $number);
    }

    /**
     * @param int|float $realNumber
     * @param int|float $imaginaryNumber
     * @param int|float $subrealNumber
     * @param int|float $subimaginaryNumber
     * @param int|float $expectedrealNumber
     * @param int|float $expectedimaginaryNumber
     *
     * @dataProvider addingValuesDataProvider
     */
    public function testIsCanAdd(
        int|float $realNumber,
        int|float $imaginaryNumber,
        int|float $subrealNumber,
        int|float $subimaginaryNumber,
        int|float $expectedrealNumber,
        int|float $expectedimaginaryNumber,
    ): void
    {
        $number = new Complex($realNumber, $imaginaryNumber);
        $subNumber = new Complex($subrealNumber, $subimaginaryNumber);
        $resultNumber = $number->add($subNumber);

        self::assertEquals($expectedrealNumber, $resultNumber->realNumber());
        self::assertEquals($expectedimaginaryNumber, $resultNumber->imaginaryNumber());
    }

    public function addingValuesDataProvider(): \Iterator
    {
        yield [0, 0, 0, 0, 0, 0];
        yield [1, 0, 0, 0, 1, 0];
        yield [1, 0, 1, 0, 2, 0];
        yield [1, 1, 1, 0, 2, 1];
        yield [1, 1, 1, 1, 2, 2];
        yield [0, 1, 0, 1, 0, 2];
        yield [0, 1, 1, 1, 1, 2];
        yield [1, 1, 0, 1, 1, 2];
        yield [1, 1, 1, 1, 2, 2];
        yield [1, 1, -1, 1, 0, 2];
        yield [1, 1, -1, -2, 0, -1];
    }

    /**
     * @param int|float $realNumber
     * @param int|float $imaginaryNumber
     * @param int|float $subrealNumber
     * @param int|float $subimaginaryNumber
     * @param int|float $expectedrealNumber
     * @param int|float $expectedimaginaryNumber
     *
     * @dataProvider subtractingValuesDataProvider
     */
    public function testIsCanSubtract(
        int|float $realNumber,
        int|float $imaginaryNumber,
        int|float $subrealNumber,
        int|float $subimaginaryNumber,
        int|float $expectedrealNumber,
        int|float $expectedimaginaryNumber,
    ): void
    {
        $number = new Complex($realNumber, $imaginaryNumber);
        $subNumber = new Complex($subrealNumber, $subimaginaryNumber);
        $resultNumber = $number->subtract($subNumber);

        self::assertEquals($expectedrealNumber, $resultNumber->realNumber());
        self::assertEquals($expectedimaginaryNumber, $resultNumber->imaginaryNumber());
    }

    public function subtractingValuesDataProvider(): \Iterator
    {
        yield [0, 0, 0, 0, 0, 0];
        yield [1, 0, 0, 0, 1, 0];
        yield [1, 0, 1, 0, 0, 0];
        yield [1, 1, 1, 0, 0, 1];
        yield [1, 1, 1, 1, 0, 0];
        yield [0, 1, 0, 1, 0, 0];
        yield [0, 1, 1, 1, -1, 0];
        yield [1, 1, 0, 1, 1, 0];
        yield [1, 1, 1, 1, 0, 0];
        yield [1, 1, -1, 1, 2, 0];
        yield [1, 1, -1, -2, 2, 3];
    }

    /**
     * @param int|float $realNumber
     * @param int|float $imaginaryNumber
     * @param int|float $subrealNumber
     * @param int|float $subimaginaryNumber
     * @param int|float $expectedrealNumber
     * @param int|float $expectedimaginaryNumber
     *
     * @dataProvider multiplyingValuesDataProvider
     */
    public function testIsCanMultiply(
        int|float $realNumber,
        int|float $imaginaryNumber,
        int|float $subrealNumber,
        int|float $subimaginaryNumber,
        int|float $expectedrealNumber,
        int|float $expectedimaginaryNumber,
    ): void
    {
        $number = new Complex($realNumber, $imaginaryNumber);
        $subNumber = new Complex($subrealNumber, $subimaginaryNumber);
        $resultNumber = $number->multiply($subNumber);

        self::assertEquals($expectedrealNumber, $resultNumber->realNumber());
        self::assertEquals($expectedimaginaryNumber, $resultNumber->imaginaryNumber());
    }

    public function multiplyingValuesDataProvider(): \Iterator
    {
        yield [0, 0, 0, 0, 0, 0];
        yield [1, 0, 0, 0, 0, 0];
        yield [1, 0, 1, 0, 1, 0];
        yield [1, 1, 1, 0, 1, 1];
        yield [1, 1, 1, 1, 0, 2];
        yield [0, 1, 0, 1, -1, 0];
        yield [0, 1, 1, 1, -1, 1];
        yield [1, 1, 0, 1, -1, 1];
        yield [1, 1, 1, 1, 0, 2];
        yield [1, 1, -1, 1, -2, 0];
        yield [1, 1, -1, -2, 1, -3];
    }

    /**
     * @param int|float $realNumber
     * @param int|float $imaginaryNumber
     * @param int|float $subrealNumber
     * @param int|float $subimaginaryNumber
     * @param int|float $expectedrealNumber
     * @param int|float $expectedimaginaryNumber
     *
     * @dataProvider dividingValuesDataProvider
     */
    public function testIsCanDivide(
        int|float $realNumber,
        int|float $imaginaryNumber,
        int|float $subrealNumber,
        int|float $subimaginaryNumber,
        int|float $expectedrealNumber,
        int|float $expectedimaginaryNumber,
    ): void
    {
        $number = new Complex($realNumber, $imaginaryNumber);
        $subNumber = new Complex($subrealNumber, $subimaginaryNumber);
        $resultNumber = $number->divide($subNumber);

        self::assertEquals($expectedrealNumber, $resultNumber->realNumber());
        self::assertEquals($expectedimaginaryNumber, $resultNumber->imaginaryNumber());
    }

    public function dividingValuesDataProvider(): \Iterator
    {
        yield [1, 0, 1, 0, 1, 0];
        yield [1, 1, 1, 0, 1, 1];
        yield [1, 1, 1, 1, 1, 0];
        yield [0, 1, 0, 1, 1, 0];
        yield [0, 1, 1, 1, 0.5, 0.5];
        yield [1, 1, 0, 1, 1, -1];
        yield [1, 1, 1, 1, 1, 0];
        yield [1, 1, -1, 1, 0, -1];
        yield [1, 1, -1, -2, -0.6, 0.2];
    }

    /**
     * @param int|float $realNumber
     * @param int|float $imaginaryNumber
     * @param int|float $subrealNumber
     * @param int|float $subimaginaryNumber
     * @param string $exceptionClass
     * @param string $exceptionMessage
     * @dataProvider dividingInvalidValuesDataProvider
     */
    public function testIsThrowExceptionOnInvalidDividing(
        int|float $realNumber,
        int|float $imaginaryNumber,
        int|float $subrealNumber,
        int|float $subimaginaryNumber,
        string    $exceptionClass,
        string    $exceptionMessage
    ): void
    {
        $this->expectException($exceptionClass);
        $this->expectExceptionMessage($exceptionMessage);

        $number = new Complex($realNumber, $imaginaryNumber);
        $subNumber = new Complex($subrealNumber, $subimaginaryNumber);
        $number->divide($subNumber);

        // заглушка
        self::assertEquals(true, true);
    }

    public function dividingInvalidValuesDataProvider(): \Iterator
    {
        yield [
            0, 0,
            0, 0,
            \DivisionByZeroError::class,
            'Division by zero',
        ];
        yield [
            1, 0,
            0, 0,
            \DivisionByZeroError::class,
            'Division by zero',
        ];
    }
}