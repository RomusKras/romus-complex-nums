<?php

declare(strict_types=1);

/**
 * VO (объект-значение) комплексного числа.
 */
class Complex {
    private int|float $realNumber;
    private int|float $imaginaryNumber;
    private string $postfix;

    public function __construct(int|float $realNumber, int|float $imaginaryNumber, string $postfix = 'i')
    {
        $this->realNumber = $realNumber;
        $this->imaginaryNumber = $imaginaryNumber;

        if ('' === trim($postfix)) {
            throw new \InvalidArgumentException("Необходимо указать постфикс.");
        }
        $this->postfix = $postfix;
    }

    public function realNumber(): int|float
    {
        return $this->realNumber;
    }

    public function imaginaryNumber(): int|float
    {
        return $this->imaginaryNumber;
    }

    public function postfix(): string
    {
        return $this->postfix;
    }

    public function __toString()
    {
        return $this->realNumber . (0 > $this->imaginaryNumber ? '' : '+') . $this->imaginaryNumber . $this->postfix;
        //return $this->realNumber . (0 > $this->imaginaryNumber ? '' : '+') . $this->imaginaryNumber . $this->postfix;
    }

    public function add(self $value): self
    {
        $realNumber = $this->realNumber + $value->realNumber();
        $imaginaryNumber = $this->imaginaryNumber + $value->imaginaryNumber();

        return new static($realNumber, $imaginaryNumber);
    }

    public function subtract(self $value): self
    {
        $realNumber = $this->realNumber - $value->realNumber();
        $imaginaryNumber = $this->imaginaryNumber - $value->imaginaryNumber();

        return new static($realNumber, $imaginaryNumber);
    }

    public function multiply(self $value): self
    {
        $realNumber = $this->realNumber * $value->realNumber() - $this->imaginaryNumber * $value->imaginaryNumber();
        $imaginaryNumber = $this->realNumber * $value->imaginaryNumber() + $this->imaginaryNumber * $value->realNumber();

        return new static($realNumber, $imaginaryNumber);
    }

    public function divide(self $value): self
    {
        $realNumber =
            ($this->realNumber * $value->realNumber() + $this->imaginaryNumber * $value->imaginaryNumber())
            /
            ($value->realNumber() * $value->realNumber() + $value->imaginaryNumber() * $value->imaginaryNumber());

        $imaginaryNumber =
            ($this->imaginaryNumber * $value->realNumber() - $this->realNumber * $value->imaginaryNumber())
            /
            ($value->realNumber() * $value->realNumber() + $value->imaginaryNumber() * $value->imaginaryNumber());

        return new static($realNumber, $imaginaryNumber);
    }
}

echo '<strong>Сложение</strong><br>';
$first = new Complex(1, 3);
$second = new Complex(4, -5);
echo $first->__toString().' и ';
echo $second->__toString().' = ';
$first = $first->add($second);
echo $first->__toString().PHP_EOL.'<br><strong>Вычитание</strong><br>';

$first = new Complex(-2, 3);
$second = new Complex(3, 2);
echo $first->__toString().' и ';
echo $second->__toString().' = ';
$first = $first->subtract($second);
echo $first->__toString().PHP_EOL.'<br><strong>Умножение</strong><br>';

$first = new Complex(1, -1);
$second = new Complex(3, 6);
echo $first->__toString().' и ';
echo $second->__toString().' = ';
$first = $first->multiply($second);
echo $first->__toString().PHP_EOL.'<br><strong>Деление</strong><br>';

$first = new Complex(13, 1);
$second = new Complex(7, -6);
echo $first->__toString().' и ';
echo $second->__toString().' = ';
$first = $first->divide($second);
echo $first->__toString().PHP_EOL;